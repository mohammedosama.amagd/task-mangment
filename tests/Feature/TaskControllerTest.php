<?php

namespace Tests\Feature;

use App\Enums\Status;
use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class TaskControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testIndex(): void
    {
        /** @var Task $task1 */
        $task1 = Task::factory()->create();

        /** @var Task $task1 */
        $task2 = Task::factory()->create();

        // Unauthorized
        $response = $this->getJson(route('tasks.index'));
        $response->assertUnauthorized();

        /** @var User $user */
        $user = User::factory()->createQuietly();

        $this->actingAs($user);

        $response = $this->getJson(route('tasks.index'));
        $response->assertOk();
        $response->assertJsonFragment($task1->toArray());
        $response->assertJsonFragment($task2->toArray());
    }

    public function testShow(): void
    {
        /** @var Task $task1 */
        $task1 = Task::factory()->create();

        // Unauthorized
        $response = $this->getJson(route('tasks.show', ['task' => $task1->id]));
        $response->assertUnauthorized();

        /** @var User $user */
        $user = User::factory()->hasAttached($task1)->createQuietly();
        $this->actingAs($user);

        $response = $this->getJson(route('tasks.show', ['task' => $task1->id]));
        $response->assertOk();
        $response->assertJsonFragment($task1->toArray());
    }

    public function testStore(): void
    {
        /** @var Task $task1 */
        $task1 = Task::factory()->make();
        $input = $task1->toArray();
        $input['assignee'] = User::factory()->count(2)->createQuietly()->pluck('id')->toArray();
        // Unauthorized
        $response = $this->postJson(route('tasks.store', []), $task1->toArray());
        $response->assertUnauthorized();

        /** @var User $user */
        $user = User::factory()->createQuietly();
        $this->actingAs($user);

        $response = $this->postJson(route('tasks.store'), $input);
        $response->assertStatus(Response::HTTP_CREATED);
        $this->assertDatabaseHas('tasks', [
            'title' => $task1->title,
            'description' => $task1->description,
            'due_date' => $task1->due_date,
            'status' => Status::Pending,
        ]);
    }

    public function testUpdate(): void
    {
        /** @var Task $task1 */
        $task1 = Task::factory()->create([
            'status' => Status::Pending,
        ]);
        $input = $task1->toArray();
        $input['assignee'] = User::factory()->count(2)->createQuietly()->pluck('id')->toArray();
        $input['status'] = Status::Completed;
        // Unauthorized
        $response = $this->putJson(route('tasks.update', ['task' => $task1->getKey()]), $input);
        $response->assertUnauthorized();

        /** @var User $user */
        $user = User::factory()->createQuietly();
        $this->actingAs($user);

        $response = $this->putJson(route('tasks.update', ['task' => $task1->getKey()]), $input);
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseHas('tasks', [
            'title' => $task1->title,
            'description' => $task1->description,
            'due_date' => $task1->due_date,
            'status' => Status::Completed,
        ]);
    }

    public function testDelete(): void
    {
        /** @var Task $task1 */
        $task1 = Task::factory()->create();

        // Unauthorized
        $response = $this->deleteJson(route('tasks.destroy', ['task' => $task1->getKey()]), $task1->toArray());
        $response->assertUnauthorized();

        /** @var User $user */
        $user = User::factory()->createQuietly();
        $this->actingAs($user);

        $response = $this->deleteJson(route('tasks.destroy', ['task' => $task1->getKey()]), $task1->toArray());
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseMissing('tasks', [
            'id' => $task1->getKey(),
            'title' => $task1->title,
            'description' => $task1->description,
            'due_date' => $task1->due_date,
        ]);
    }
}
