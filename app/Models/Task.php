<?php

namespace App\Models;

use App\Enums\Status;
use Carbon\CarbonImmutable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * class Task
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property CarbonImmutable $due_date
 * @property Status $status
 * @property string|null $image
 * @property CarbonImmutable $created_at
 * @property CarbonImmutable $updated_at
 * @property Collection<User> $users
 */
class Task extends Model
{
    use HasFactory;

    public $casts = [
        'due_date' => 'datetime',
        'status' => Status::class,
    ];

    protected $fillable = [
        'title',
        'description',
        'due_date',
        'status',
    ];

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'task_assignee', 'task_id', 'user_id');
    }

    public function getImageAttribute($value)
    {
        return $value ? url($value) : null;
    }
}
