<?php

namespace App\Observers;

use App\Events\TaskStatusUpdated;
use App\Models\Task;

class TaskObserver
{
    /**
     * Handle the Task "updated" event.
     */
    public function updated(Task $task): void
    {
        if ($task->isDirty('status')) {
            event(new TaskStatusUpdated($task));
        }
    }
}
