<?php

namespace App\Listeners;

use App\Events\TaskStatusUpdated;
use App\Mail\TaskStatusUpdateEmail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendTaskEmail implements ShouldQueue
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(TaskStatusUpdated $event): void
    {
        if ($event->task->users->isEmpty()) {
            return;
        }
        Mail::send(new TaskStatusUpdateEmail($event->task));
    }
}
