<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    public function login(LoginRequest $request): JsonResponse
    {
        $credentials = $request->only('email', 'password');

        if (! Auth::attempt($credentials)) {
            return response()->json([
                'message' => 'Invalid credentials',
            ], 401);
        }

        $user = User::query()->where('email', $credentials['email'])->first();

        $token = $user->createToken('authToken')->plainTextToken;

        return new JsonResponse([
            'user' => $user,
            'token' => $token,
        ]);
    }

    public function register(RegisterRequest $request): JsonResponse
    {
        $user = new User();
        $user->name = $request->validated('name');
        $user->email = $request->validated('email');
        $user->password = $request->validated('password');
        $user->save();

        $token = $user->createToken('authToken')->plainTextToken;

        return new JsonResponse([
            'user' => $user,
            'token' => $token,
        ], 201);
    }

    public function profile(Request $request): JsonResponse
    {
        return new JsonResponse(Auth::user()->load('tasks'));
    }
}
