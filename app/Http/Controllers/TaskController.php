<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Models\Task;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;

class TaskController extends Controller
{
    public function index(Request $request): JsonResponse
    {
        $tasks = Task::query()->with('users');
        $search = $request->query('search');
        if ($search) {
            $tasks->where(function ($query) use ($search) {
                $query->orWhere('title', 'like', "%{$search}%")
                    ->orWhere('description', 'like', "%{$search}%");
            });
        }
        $tasks = $tasks->get();

        return new JsonResponse($tasks);
    }

    public function show(Task $task): JsonResponse
    {
        return new JsonResponse($task);
    }

    public function store(CreateTaskRequest $request): JsonResponse
    {
        $task = new Task();
        $task->title = $request->validated('title');
        $task->description = $request->validated('description');
        $task->due_date = $request->validated('due_date');
        if ($request->validated('image')) {
            $task->image = $this->uploadTaskImage($task, $request->validated('image'));
        }
        $task->save();

        $task->users()->attach($request->validated('assignee'));

        return new JsonResponse($task, Response::HTTP_CREATED);
    }

    public function update(UpdateTaskRequest $request, Task $task): JsonResponse
    {
        $task->title = $request->validated('title');
        $task->description = $request->validated('description');
        $task->due_date = $request->validated('due_date');
        $task->status = $request->validated('status');
        if ($request->validated('image')) {
            $task->image = $this->uploadTaskImage($task, $request->validated('image'));
        }
        $task->save();

        if ($request->has('assignee')) {
            $task->users()->sync($request->validated('assignee'));
        }

        return new JsonResponse($task, Response::HTTP_OK);
    }

    public function destroy(Task $task): JsonResponse
    {
        $success = $task->delete();

        return new JsonResponse($success, Response::HTTP_OK);
    }

    protected function uploadTaskImage(Task $task, UploadedFile $image): string
    {
        if ($task->image) {
            Storage::delete($task->getRawOriginal('image'));
        }
        $path = 'public/tasks';
        $fileName = $image->hashName();
        $image->storeAs($path, $fileName);

        return $path.'/'.$fileName;
    }
}
